import shutil
import os

# Source template path
destination = "parameters.json"

# Destination template path
source= "ParamtersCFT.json"

try:
    shutil.copy(source, destination)
    print("File copied successfully.")

except:
    print("Error occurred while copying file.")

#     ************************* Deleteing ParametersCFT.json  *************************

if os.path.exists(source):
    os.remove(source)
else:
    print("The file does not exist")